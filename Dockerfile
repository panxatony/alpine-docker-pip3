FROM docker:latest

RUN apk add --no-cache python3 curl python pkgconfig python-dev openssl-dev libffi-dev musl-dev make gcc python2-dev python3-dev dev86 linux-headers && \
    python3 -m ensurepip && \
    rm -r /usr/lib/python*/ensurepip && \
    pip3 install --upgrade pip setuptools && \
    pip3 install docker-py && \
    pip3 install ansible molecule && \
    if [ ! -e /usr/bin/pip ]; then ln -s pip3 /usr/bin/pip ; fi && \
    if [[ ! -e /usr/bin/python ]]; then ln -sf /usr/bin/python3 /usr/bin/python; fi && \
    rm -r /root/.cache